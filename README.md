# F<sup>2</sup>B
F<sup>2</sup>B (Free Fencing Buzzer) is an open-source, ATTiny based Fencing Buzzer (similar to [this one](http://www.blue-gauntlet.com/Italian-Epee-BUZZER-BOX_p_3846.html)).

## License
This is released under the [GPL 3](http://www.gnu.org/licenses/gpl-3.0.en.html) license.

## Warning
I will not maintain this project after the 25th of February 2017.

## The files
### eagle/
These are the [Eagle](http://www.autodesk.com/products/eagle/overview) schematics for the beeper. The PCB is designed as a "shield" for the [digispark attiny development board](http://www.ebay.com/itm/1-2-5-10PCS-Digispark-Kickstarter-Attiny85-USB-Development-Board-Module-Arduino-/272345523643?var=&hash=item3f690ee5bb:m:mGBzq4XceS0-QCqCkXNaYcQ).

### arduino/
This is the Arduino sketch.

### 3d/
These are the Sketchup projects for the case. The case is designed to support the electronics and one 9V battery. This is meant to be 3D printed.
#### /3d/skp8/
These are the Sketchup Version 8 3d files.

## The code
To setup the code you must do the following:
1. Grab the ATTiny board support for the Arduino IDE:
  1. Open up the Arduino IDE. Go to File->Preferences and in the Additional Boards Manager URLs enter the following link: `www.raw.githubusercontent.com/damellis/attiny/ide-1.6.x-boards-manager/package_damellis_attiny_index.json`
  2. Open up the Boards Manager by going to Tools->Board->Board Manager.
  3. Install the `attiny` library.
  4. If you wish to get a more in-depth explanation, I recommend visiting [this link](http://www.github.com/damellis/attiny).
2. Set the board to the `ATTiny 25/45/85`, and set the clock speed to whatever. I recommend `1MHz` as this will be the most power efficient.
3. Set the `BUTTON_PIN` and `BUZZER_PIN` accordingly (by default they are `0` and `1`, respectively).
4. Set the `DELAY` to how many milliseconds you want it to buzz (by default `3000`).
5. Set the `CLOCK_SPEED` to how many MHz you are compiling the code for (`1`, `8` or `16`, by default `1`).
6. If you wish to Upload the code to the ATTiny via an UNO, I recommend visiting [this link](http://www.highlowtech.org/?p=1695).