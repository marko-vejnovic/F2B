/*
	F2B - A free fencing buzzer.
	Copyright (C) 2017 Marko Vejnovic

	This program is free software: you can redistribute it and/or modify
	it under the terms of the GNU General Public License as published by
	the Free Software Foundation, either version 3 of the License, or any later
	version.

	This program is distributed in the hope that it will be useful,
	but WITHOUT ANY WARRANTY; without even the implied warranty of
	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
	GNU General Public License for more details.

	You should have received a copy of the GNU General Public License
	along with this program.  If not, see <http://www.gnu.org/licenses/>.

	If you wish to contact Marko Vejnovic, do so by emailing 
	marko.vejnovic@hotmail.com
*/

#define BUTTON_PIN 0
#define BUZZER_PIN 1
#define DELAY 3000 // Period of the beep
#define CLOCK_SPEED 1 // Compiled clock speed in MHz

#define CLOCKED_DELAY 16 * DELAY / CLOCK_SPEED

void setup()
{
	pinMode(BUTTON_PIN, INPUT);
	pinMode(BUZZER_PIN, OUTPUT);
}

void loop()
{
	int btnValue = digitalRead(BUTTON_PIN);

	if (btnValue == HIGH)
	{
		digitalWrite(BUZZER_PIN, HIGH);
		delay(CLOCKED_DELAY);
	}

	digitalWrite(BUZZER_PIN, LOW);
}
